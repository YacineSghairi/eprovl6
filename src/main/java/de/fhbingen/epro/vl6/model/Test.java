package de.fhbingen.epro.vl6.model;

/**
 * @author Johannes Hiemer.
 *
 */
public class Test {

	private String name;
	
	private String description;
	
	public Test() {
		super();
	}

	public Test(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
