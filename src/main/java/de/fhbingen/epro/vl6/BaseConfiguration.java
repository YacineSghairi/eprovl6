package de.fhbingen.epro.vl6;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "de.fhbingen.epro.vl6.queue" })
public class BaseConfiguration {
}
