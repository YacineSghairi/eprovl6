package de.fhbingen.epro.vl6.queue.configuration;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import de.fhbingen.epro.vl6.queue.error.ExecuteErrorHandler;
import de.fhbingen.epro.vl6.queue.listener.ApplicationMessageListener;


/**
 * @author Johannes Hiemer.
 *
 */
@Component
public class ApplicationQueueConfiguration {
	
	@Autowired
	private ConnectionFactory connectionFactory;
	
	@Autowired
	private ExecuteErrorHandler executeErrorHandler;
	
	@Autowired
	private ApplicationMessageListener applicationMessageListener;
	
	@Autowired
	private AmqpAdmin amqpAdmin;
	
	private final static String QUEUE_NAME = "demo.queue";
	
	private final static String EXCHANGE_NAME = "demo.exchange";
	
	private final static String ROUTING_KEY = "demo.rtg.key";
	
	@Bean
	public SimpleMessageListenerContainer applicationMessageListenerContainer() {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueues(applicationQueue());
		container.setMessageListener(applicationMessageListener);
		container.setErrorHandler(executeErrorHandler);
		container.setAutoStartup(true);
		container.setConcurrentConsumers(2);
		return container;
	}
	
	@Bean
	public Queue applicationQueue() {
		Queue queue = new Queue(QUEUE_NAME, true);
		amqpAdmin.declareQueue(queue);
		
		return queue;
	}
	
	@Bean
	public DirectExchange applicationExchange() {
		DirectExchange directExchange = new DirectExchange(EXCHANGE_NAME, true, true);
		amqpAdmin.declareExchange(directExchange);
		
		return directExchange; 
	}
	
	@Bean
	public Binding applicationBinding() {
		Binding binding = BindingBuilder.bind(applicationQueue()).to(applicationExchange()).with(ROUTING_KEY);
		amqpAdmin.declareBinding(binding);
		
		return binding;
	}
	
}
