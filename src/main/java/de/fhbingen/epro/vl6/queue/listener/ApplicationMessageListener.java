package de.fhbingen.epro.vl6.queue.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fhbingen.epro.vl6.model.Test;
import de.fhbingen.epro.vl6.queue.error.ExecuteErrorHandler;

/**
 * @author Johannes Hiemer.
 *
 */
@Component
public class ApplicationMessageListener implements MessageListener {
	
	private static final Logger log = LoggerFactory
            .getLogger(ApplicationMessageListener.class);
	
	@Autowired
	private ExecuteErrorHandler executeErrorHandler;
	
	Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();

	/* (non-Javadoc)
	 * @see org.springframework.amqp.core.MessageListener#onMessage(org.springframework.amqp.core.Message)
	 */
	@Override
	public void onMessage(Message message) {
		Test test = null;
		try {
			test = (Test) messageConverter.fromMessage(message);
			
			log.info("Received message at: " + test.getName());
		} catch(Exception ex) {
			executeErrorHandler.handleError(test, ex);
		}
		
	}
	
}
